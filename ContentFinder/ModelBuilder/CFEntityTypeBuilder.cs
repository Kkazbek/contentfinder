﻿using System;
using System.Collections.Generic;
using System.Text;
using ContentFinder.Core;
using ContentFinder.ModelBuilder.Internal;

namespace ContentFinder.ModelBuilder
{
    public class CFEntityTypeBuilder<TEntity> where TEntity : class
    {
        private CFSet<TEntity> _set = new CFSet<TEntity>();
        private readonly CFContext _context;

        public CFEntityTypeBuilder(CFContext context)
        {
            _set = (CFSet<TEntity>)context.GetOrAddSet(_set, _set.EntityType);
            _context = context;
        }

        public CFAsscoiatedPageBuilder<TEntity> ListedIn(string uriPattern)
        {
            _set.AssociatedPages = new AssociatedPages(uriPattern);
            _set.singleUri = null;

            return new CFAsscoiatedPageBuilder<TEntity>(_set);
        }

        public CFEntityTypeBuilder<TEntity> IsSinglePage(string uri)
        {
            _set.AssociatedPages = null;
            _set.singleUri = uri;

            return this;
        }

        public CFEntityTypeBuilder<TEntity> HasDescendant<TDescendant>() where TDescendant: class
        {
            var descendant = (CFSet<TDescendant>)_context.GetOrAddSet(new CFSet<TDescendant>(), typeof(TDescendant));
            if (descendant.Parent != null)
            {
                throw new ArgumentException($"У сущности {typeof(TDescendant).Name} уже существует предок");
            }
            if (descendant.isRoot)
            {
                throw new ArgumentException($"Сущность {typeof(TDescendant).Name} не может иметь предков, так как является корневой");
            }
            if (descendant.EntityInstance == _set.EntityInstance)
            {
                throw new ArgumentException($"Сущность {typeof(TDescendant).Name} ссылается на саму себя");
            }

            try
            {
                _set.descendants.Add(descendant);
            }
            catch (UriFormatException e)
            {
                throw new ArgumentException($"Неверный формат ссылки при попытке добавить дочернюю сущность {typeof(TDescendant).Name}", e);
            }
            descendant.Parent = _set;
            
            return this;
        }

        public CFEntityTypeBuilder<TEntity> HasContainerContent(string contentContainer)
        {
            _set.contentContainer = contentContainer;

            return this;
        }

        public CFEntityTypeBuilder<TEntity> IsRoot()
        {
            foreach (var set in _context.SetsDictionary)
            {
                if (set.Value.IsRoot())
                {
                    throw new ArgumentException($"В модели уже объявлена корневая сущность {set.Value.EntityType.Name}");
                }
            }
            if (_set.Parent == null)
            {
                _set.SetRoot();
            }
            else
            {
                throw new ArgumentException($"Данная сущность не может стать корневой, так она уже имеет предка {_set.Parent.GetType().Name}");
            }
            
            return this;
        }

        public CFPropertyTypeBuilder<TEntity, TResult> Property<TResult>(Func<TEntity, TResult> selector)
        {
            return new CFPropertyTypeBuilder<TEntity, TResult>(_set, selector);
        }


    }
}
