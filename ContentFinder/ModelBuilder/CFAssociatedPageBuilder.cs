﻿using ContentFinder.ModelBuilder.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace ContentFinder.ModelBuilder
{
    public class CFAsscoiatedPageBuilder<TEntity> where TEntity : class
    {
        private readonly CFSet<TEntity> _set;
        public CFAsscoiatedPageBuilder(CFSet<TEntity> set)
        {
            _set = set;
        }

        public CFAsscoiatedPageBuilder<TEntity> HasLinkSelector(string linkSelector)
        {
            _set.AssociatedPages.LinkSelector = linkSelector;

            return this;
        }

        public CFAsscoiatedPageBuilder<TEntity> SetLimitBlock(string containerBlock)
        {
            _set.AssociatedPages.ContentBlock = containerBlock;

            return this;
        }
    }
}
