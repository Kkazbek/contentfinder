﻿using System;
using System.Collections.Generic;
using System.Text;
using ContentFinder.ModelBuilder.Internal;

namespace ContentFinder.ModelBuilder
{
    public class CFModelBuilder
    {
        private CFContext _context;

        public CFModelBuilder(CFContext context)
        {
            _context = context ?? throw new ArgumentNullException();
        }

        public CFEntityTypeBuilder<TEntity> Entity<TEntity>() where TEntity: class
        {
            return new CFEntityTypeBuilder<TEntity>(_context);
        }
    }
}
