﻿using System;
using System.Collections.Generic;
using System.Text;
using ContentFinder.ModelBuilder.Internal;

namespace ContentFinder.ModelBuilder
{
    public class CFContextOptionsBuilder
    {
        private CFContextOptions _options;

        public CFContextOptionsBuilder(CFContextOptions options)
        {
            _options = options;
        }

        public CFContextOptionsBuilder UseUri(Uri uri)
        {
            _options.Uri = uri;

            return this;
        }

        public CFContextOptionsBuilder ToJson(string filename)
        {
            _options.jsonFilename = filename;

            return this;
        }
    }
}
