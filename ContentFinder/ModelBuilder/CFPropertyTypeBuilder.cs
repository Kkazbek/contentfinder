﻿using ContentFinder.Core;
using ContentFinder.ModelBuilder.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace ContentFinder.ModelBuilder
{
    public class CFPropertyTypeBuilder<TEntity, TResult> where TEntity: class
    {
        private readonly CFSet<TEntity> _set;
        private CFProperty _property;

        public CFPropertyTypeBuilder(CFSet<TEntity> set, Func<TEntity, TResult> selector)
        {
            _set = set;
            foreach (var prop in _set.EntityInstance.GetType().GetProperties())
            {
                // Проверяем ссылки или если это примитивный тип, то значения
                if (prop.GetValue(_set.EntityInstance) == (object)selector(_set.EntityInstance) || (prop.PropertyType.IsPrimitive && prop.GetValue(_set.EntityInstance).ToString() == selector(_set.EntityInstance).ToString()))
                {
                    _property = (CFProperty)set.Properties[prop];
                    break;
                }
            }
            _property = _property ?? throw new ArgumentException($"Для сущности {typeof(TEntity).Name} была сделана попытка использовать свойство, которое в нем не определено");
        }

        public CFPropertyTypeBuilder<TEntity, TResult> HasSelector(string selector)
        {
            _property.Selectors.Add(selector);

            return this;
        }

        public CFPropertyTypeBuilder<TEntity, TResult> Select(Func<Content, TResult> selector)
        {
            Func<Content, object> func = (c) => { return (object)selector(c); };
            _property.extractData = func;

            return this;
        }

        public CFPropertyTypeBuilder<TEntity, TProperty> Property<TProperty>(Func<TEntity, TProperty> selector)
        {
            return new CFPropertyTypeBuilder<TEntity, TProperty>(_set, selector);
        }

    }
}
