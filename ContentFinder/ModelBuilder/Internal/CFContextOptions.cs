﻿using System;
using ContentFinder.Core;

namespace ContentFinder.ModelBuilder.Internal
{
    public class CFContextOptions
    {
        private CFContext _context;
        internal string jsonFilename = "";

        public Uri Uri { get; set; }

        public CFContextOptions(CFContext context)
        {
            _context = context;
        }

        public void Apply()
        {
            Page.host = $"{Uri.Scheme}://{Uri.Host}";
        }
    }
}