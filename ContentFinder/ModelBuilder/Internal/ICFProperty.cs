﻿using ContentFinder.Core;
using System;
using System.Collections;
using System.Text;

namespace ContentFinder.ModelBuilder.Internal
{
    public interface ICFProperty
    {
        object ExtractData(Content content);
        string PropertyName { get; }
    }
}
