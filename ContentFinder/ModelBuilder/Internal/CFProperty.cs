﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using ContentFinder.Core;

namespace ContentFinder.ModelBuilder.Internal
{
    public class CFProperty : ICFProperty<object>
    {
        private string _name;
        private List<string> _selectors = new List<string>();

        internal Func<Content, object> extractData;

        public List<string> Selectors
        {
            get => _selectors;
            internal set => _selectors = value;
        }

        public string PropertyName
        {
            get
            {
                return _name;
            }
        }

        public CFProperty(PropertyInfo propertyInfo)
        {
            _name = propertyInfo.Name;
        }

        public object ExtractData(Content content)
        {
            return extractData(content);
        }
    }
}
