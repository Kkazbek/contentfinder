﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Text;
using System.Linq;

namespace ContentFinder.ModelBuilder.Internal
{
    public abstract class CFContext
    {
        private IDictionary<Type, ICFSet> _sets = new Dictionary<Type, ICFSet>();
        private readonly CFContextOptions _options;

        internal IDictionary<Type, ICFSet> SetsDictionary
        {
            get
            {
                return _sets;
            }
        }

        public IReadOnlyDictionary<Type, ICFSet> Sets { get; }

        internal ICFSet GetOrAddSet(ICFSet source, Type type)
        {
            if (!_sets.TryGetValue(type, out var set))
            {
                set = source;
                _sets[type] = source;
            }

            return set;
        }

        public CFContextOptions Options { get => _options; }

        public CFContext()
        {
            InitializeReflectedSets();

            _options = new CFContextOptions(this);

            var optionBuilder = new CFContextOptionsBuilder(_options);
            OnConfiguring(optionBuilder);
            _options.Apply();

            var modelBuilder = new CFModelBuilder(this);
            OnModelCreating(modelBuilder);

            FillReflectedSets();
            Sets = new ReadOnlyDictionary<Type, ICFSet>(_sets);
        }

        private void FillReflectedSets()
        {
            var reflSets = from prop in GetType().GetConstructor(new Type[] { }).ReflectedType.GetProperties()
                           where prop.PropertyType.GetInterface(nameof(ICFSet)) == typeof(ICFSet)
                           select prop;
            foreach (var prop in reflSets)
            {
                prop.SetValue(this, _sets[prop.PropertyType.GenericTypeArguments.First()]);
            }
        }

        private void InitializeReflectedSets()
        {
            foreach (var prop in GetType().GetConstructor(new Type[] { }).ReflectedType.GetProperties())
            {
                if (prop.PropertyType.GetInterface(nameof(ICFSet)) == typeof(ICFSet))
                {
                    prop.SetValue(this, prop.PropertyType.GetConstructor(new Type[] { }).Invoke(new object[] { }));
                    GetOrAddSet((ICFSet)prop.GetValue(this), prop.PropertyType.GenericTypeArguments[0]);
                }
            }
        }

        protected virtual void OnConfiguring(CFContextOptionsBuilder optionsBuilder) => throw new NotImplementedException();

        protected virtual void OnModelCreating(CFModelBuilder modelBuilder) => throw new NotImplementedException();
    }
}
