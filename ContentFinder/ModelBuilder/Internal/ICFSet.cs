﻿using System;
using System.Collections;
using System.Text;
using System.Collections.Generic;
using System.Reflection;
using ContentFinder.Core;

namespace ContentFinder.ModelBuilder.Internal
{
    public interface ICFSet: IList
    {
        string SingleUri { get; }
        AssociatedPages AssociatedPages { get; set; }
        void AddEntity(Link link, object entity);
        IDictionary<Link, object> Entities { get; set; }
        IReadOnlyDictionary<PropertyInfo, ICFProperty<object>> Properties { get; }
        IReadOnlyList<ICFSet> Descendants { get; }
        Type EntityType { get; }
        object EntityInstance { get; }
        bool IsRoot();
        void SetRoot();
    }
}
