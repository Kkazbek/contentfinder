﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using ContentFinder.Core;

namespace ContentFinder.ModelBuilder.Internal
{
    public class CFProperty<TResult> : CFProperty, ICFProperty<TResult>
    {
        internal new Func<Content, TResult> extractData;

        public CFProperty(PropertyInfo propertyInfo) : base(propertyInfo)
        {
        }

        public new TResult ExtractData(Content content)
        {
            return extractData(content);
        }

    }
}
