﻿using ContentFinder.Core;
using System;
using System.Collections.Generic;

namespace ContentFinder.ModelBuilder.Internal
{
    public interface ICFSet<TEntity> : ICFSet, IList<TEntity> where TEntity: class
    {
        new TEntity EntityInstance { get; }
        new IDictionary<Link, TEntity> Entities { get; set; }
        void AddEntity(Link link, TEntity entity);
    }
}