﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Collections.ObjectModel;
using System.Collections;
using ContentFinder.Core;
using System.Linq;

namespace ContentFinder.ModelBuilder.Internal
{
    public class CFSet<TEntity> : List<TEntity>, ICFSet<TEntity> where TEntity : class
    {
        #region BadCode
        private int __intPropertyCounter = 0; // служит для присваивания свойствам типа int значений по умолчанию
        private int __IntPropertyCounter => __intPropertyCounter++;
        #endregion

        internal bool isRoot;
        internal string contentContainer;
        internal string singleUri;
        internal List<ICFSet> descendants = new List<ICFSet>();
        private Dictionary<PropertyInfo, ICFProperty<object>> _properties = new Dictionary<PropertyInfo, ICFProperty<object>>();
        private TEntity _instance;
        private ICFSet _parent;
        private static readonly object _lock = new object();
        private AssociatedPages _associatedPages;
        private Dictionary<Link, TEntity> _entities = new Dictionary<Link, TEntity>();

        public Type EntityType => typeof(TEntity);
        object ICFSet.EntityInstance => EntityInstance;
        public IReadOnlyDictionary<PropertyInfo, ICFProperty<object>> Properties { get; }
        public IReadOnlyList<ICFSet> Descendants { get; }

        public CFSet()
        {
            // Инициализация свойств синглтона EntityInstance
            foreach (var prop in typeof(TEntity).GetProperties())
            {
                _properties[prop] = new CFProperty(prop);
                #region BadCode
                if (prop.PropertyType == typeof(string))
                {
                    prop.SetValue(EntityInstance, prop.Name);
                }
                if (prop.PropertyType == typeof(int))
                {
                    prop.SetValue(EntityInstance, __IntPropertyCounter);
                }
                if (prop.PropertyType.GetInterfaces().Where(i => i.Name == "IList").Count() != 0)
                {
                    prop.SetValue(EntityInstance, prop.PropertyType.GetConstructor(new Type[] { }).Invoke(new object[] { }));
                }
                #endregion
            }
            // Инициализация коллекций только для чтения
            Properties = new ReadOnlyDictionary<PropertyInfo, ICFProperty<object>>(_properties);
            Descendants = new ReadOnlyCollection<ICFSet>(descendants);
        }

        public string SingleUri { get => singleUri; }
        public AssociatedPages AssociatedPages { get => _associatedPages; set => _associatedPages = value; }

        internal ICFSet Parent
        {
            get
            {
                return _parent;
            }
            set
            {
                if (isRoot)
                {
                    throw new ArgumentException($"Сущность {typeof(TEntity).Name} является корневой, поэтому она не может иметь родителей");
                }
                _parent = value;
            }
        }

        public TEntity EntityInstance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_lock)
                    {
                        if (_instance == null)
                        {
                            _instance = (TEntity)(typeof(TEntity).GetConstructor(new Type[] { }).Invoke(new object[] { }));
                        }
                    }
                }
                return _instance;
            }
        }

        public IDictionary<Link, TEntity> Entities { get => _entities; set => _entities = (Dictionary<Link, TEntity>)value; }
        IDictionary<Link, object> ICFSet.Entities { get => _entities.Select(ent => new KeyValuePair<Link, object>(ent.Key, (object)ent.Value)).ToDictionary(ent => ent.Key, ent => ent.Value); set => _entities = (Dictionary<Link, TEntity>)value; }

        public void SetRoot()
        {
            isRoot = true;
            _parent = null;
        }

        public bool IsRoot()
        {
            return isRoot;
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public override string ToString()
        {
            return base.ToString();
        }

        public void AddEntity(Link link, TEntity entity)
        {
            this.Add(entity);
            Entities.Add(link, entity);
        }

        public void AddEntity(Link link, object entity)
        {
            AddEntity(link, (TEntity)entity);
        }
    }
}
