﻿using ContentFinder.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace ContentFinder.ModelBuilder.Internal
{
    public interface ICFProperty<TResult> : ICFProperty
    {
        new TResult ExtractData(Content content);
    }
}
