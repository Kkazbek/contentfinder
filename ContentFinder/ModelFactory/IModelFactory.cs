﻿using ContentFinder.ModelBuilder.Internal;
using System;
using System.Collections.Generic;
using System.Text;

namespace ContentFinder.ModelFactory
{
    public interface IModelFactory<TContext> where TContext : CFContext
    {
        TContext Build(bool saveToFile);
    }
}
