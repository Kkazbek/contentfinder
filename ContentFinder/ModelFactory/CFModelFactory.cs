﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ContentFinder.ModelBuilder.Internal;
using ContentFinder.Core;
using System.Reflection;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.IO;

namespace ContentFinder.ModelFactory
{
    public class CFModelFactory<TContext> : IModelFactory<TContext> where TContext : CFContext, new()
    {
        private TContext context;
        private ICFSet rootSet;
        private Regex hostExpr;
        private string hostRef              = @"{this}";
        private string parentRef            = @"{parent}";
        private string parentReplOperator   = @"\[(\s*'\w+'\s*->\s*'\w+'\s*)\]";
        private Regex hostRefExpr;
        private Regex parentRefExpr;
        private Regex parentReplRefExpr;
        private Dictionary<Regex, string> replacements = new Dictionary<Regex, string>();

        public CFModelFactory()
        {
            context             = new TContext();
            hostExpr            = new Regex($"{context.Options.Uri.Scheme}://{context.Options.Uri.Host}");
            hostRefExpr         = new Regex(hostRef);
            parentRefExpr       = new Regex(parentRef);
            parentReplRefExpr   = new Regex(parentReplOperator);
            rootSet             = ( from set in context.Sets
                                    where set.Value.IsRoot()
                                    select set.Value ).First();
        }

        public TContext Build(bool saveToFile = true)
        {
            FillModel(rootSet);

            if (saveToFile)
            {
                SaveToFile(context.Options.jsonFilename, rootSet.Entities.Values);
            }

            return context;
        }

        public void FillModel(ICFSet set, ICFSet parent = null)
        {
            
            if (set.AssociatedPages != null)
            {
                // Ставим ссылку вместо {this}
                set.AssociatedPages.UriPattern = hostRefExpr.Replace(set.AssociatedPages.UriPattern, $"{context.Options.Uri.Scheme}://{context.Options.Uri.Host}");
                if (parent != null)
                {
                    string uriBuff = set.AssociatedPages.UriPattern;
                    foreach (var parentEnt in parent.Entities)
                    {
                        // Ставим ссылку вместо {parent}
                        if (parentRefExpr.IsMatch(uriBuff))
                        {
                            set.AssociatedPages.UriPattern = parentRefExpr.Replace(uriBuff, hostExpr.Replace(parentEnt.Key.Uri, ""));
                        }
                        else
                        {
                            var parentReplExpr = new Regex(@"{parent\[(\s*'\w+'\s*->\s*'\w+'\s*)\]}");
                            set.AssociatedPages.UriPattern = parentReplExpr.Replace(uriBuff, hostExpr.Replace(parentEnt.Key.Uri, ""));
                        }

                        // Если в {parent} есть замены
                        foreach (Match match in parentReplRefExpr.Matches(uriBuff))
                        {
                            string[] words = match.Value.Split(new char[] { '\'' });
                            replacements[new Regex(words[1])] = words[3];
                        }
                        foreach (var replacement in replacements)
                        {
                            replacement.Key.Replace(set.AssociatedPages.UriPattern, replacement.Value);
                        }
                        replacements.Clear();

                        List<object> entities = FillSet(set);

                        var childListProp = (from prop in parentEnt.Value.GetType().GetProperties()
                                             where prop.PropertyType.Name == "List`1" &&
                                                    prop.PropertyType.GenericTypeArguments.First() == set.EntityType
                                             select prop).First();
                        var childList = childListProp.PropertyType.GetConstructor(new Type[] { }).Invoke(new object[] { });

                        foreach (var entity in entities)
                        {
                            childListProp.PropertyType.GetMethod("Add").Invoke(childList, new object[] { entity });
                        }

                        childListProp.SetValue(parentEnt.Value, childList);
                    }
                }
                else
                {
                    FillSet(set);
                }
            }
            else
            {

                string singleUri = hostRefExpr.Replace(set.SingleUri, $"{context.Options.Uri.Scheme}://{context.Options.Uri.Host}");
                if (parent != null)
                {
                    string uriBuff = singleUri;
                    foreach (var parentEnt in parent.Entities)
                    {
                        // Ставим ссылку вместо {parent}
                        if (parentRefExpr.IsMatch(uriBuff))
                        {
                            singleUri = parentRefExpr.Replace(uriBuff, hostExpr.Replace(parentEnt.Key.Uri, ""));
                        }
                        else
                        {
                            var parentReplExpr = new Regex(@"{parent\[(\s*'\w+'\s*->\s*'\w+'\s*)\]}");
                            singleUri = parentReplExpr.Replace(uriBuff, hostExpr.Replace(parentEnt.Key.Uri, ""));
                        }

                        // Если в {parent} есть замены
                        foreach (Match match in parentReplRefExpr.Matches(uriBuff))
                        {
                            string[] words = match.Value.Split(new char[] { '\'' });
                            replacements[new Regex(words[1])] = words[3];
                        }
                        foreach (var replacement in replacements)
                        {
                            singleUri = replacement.Key.Replace(singleUri, replacement.Value);
                        }
                        replacements.Clear();

                        Link link = new Link() { Uri = singleUri };
                        var entity = GetWebContent(link.Uri, set);
                        set.AddEntity(link, entity);

                        var childListProp = (from prop in parentEnt.Value.GetType().GetProperties()
                                             where prop.PropertyType.Name == "List`1" &&
                                                    prop.PropertyType.GenericTypeArguments.First() == set.EntityType
                                             select prop).First();
                        var childList = childListProp.PropertyType.GetConstructor(new Type[] { }).Invoke(new object[] { });

                        childListProp.PropertyType.GetMethod("Add").Invoke(childList, new object[] { entity });

                        childListProp.SetValue(parentEnt.Value, childList);

                        Console.WriteLine("Сущность добавлена");
                    }
                }
                else
                {
                    Link link = new Link() { Uri = singleUri };

                    set.AddEntity(link, GetWebContent(link.Uri, set));

                    Console.WriteLine("Сущность добавлена");
                }
            }

            if (set.Descendants.Count == 0)
            {
                return;
            }
            else
            {
                foreach (var descSet in set.Descendants)
                {
                    FillModel(descSet, set);
                }
            }
        }

        private List<object> FillSet(ICFSet set)
        {
            set.AssociatedPages.Clear();
            set.AssociatedPages.Links.Clear();
            set.AssociatedPages.Fill((page) =>
            {
                Console.WriteLine($"Ссылка {page.LinkToPage.ToString()} добавлена");
            });
            List<Link> links = set.AssociatedPages.GetLinks().Links;
            List<object> entities = new List<object>();
            foreach (var link in links)
            {
                object entity = GetWebContent(link.Uri, set);
                entities.Add(entity);
                set.AddEntity(link, entity);

                Console.WriteLine("Сущность добавлена");
            }
            return entities;
        }

        public object GetWebContent(string uri, ICFSet set)
        {
            Page page = new Page(uri);
            var entity = GetEntity(set);
            var htmlDocument = page.HtmlDocument.DocumentNode;
            foreach (var entityProp in set.Properties)
            {
                foreach (var selector in (entityProp.Value as CFProperty).Selectors)
                {
                    Content content = new Content().GetContent(htmlDocument, selector);
                    if (content.HtmlElements == null)
                    {
                        continue;
                    }
                    if ((entityProp.Value as CFProperty).extractData != null)
                    {
                        entityProp.Key.SetValue(entity, entityProp.Value.ExtractData(content));
                        break;
                    }
                    else
                    {
                        string textContent = "";
                        foreach (var elem in content.HtmlElements)
                        {
                            textContent += elem.InnerText;
                        }
                        // PropName = PropName + Content
                        entityProp.Key.SetValue(entity, textContent + entityProp.Key.GetValue(entity));
                    }
                }
            }
            return entity;
        }

        public object GetEntity(ICFSet set)
        {
            // Получаем пустой конструктор класса сущности
            var entityConstructor = set.EntityType.GetConstructor(new Type[] { });
            // Вызываем конструктор
            return entityConstructor.Invoke(new object[] { });
        }       

        public void SaveToFile(string fileName, object value)
        {
            JsonSerializer jsonSerializer = new JsonSerializer();
            using (StreamWriter streamWriter = new StreamWriter(fileName))
            using (JsonWriter jsonWriter = new JsonTextWriter(streamWriter))
            {
                jsonSerializer.Serialize(jsonWriter, value);
            }
        }
    }
}
