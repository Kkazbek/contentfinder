﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;


namespace ContentFinder.Core
{
    /// <summary>
    /// <para>Класс для работы со множеством одинаковых страниц</para>
    /// </summary>
    /// <remarks>
    /// <para>Некоторые страницы связаны друг с другом, например, при их навигации между собой,
    /// которые находятся на одном уровне(то есть основная структура страниц одинаковая).</para>
    /// <para>Данный класс предоставляет возможность работать с такими страницами как с одним целым</para>
    /// </remarks>
    /// <typeparam name="Page">Указывает с каким типом страниц будет работать класс</typeparam>
    /// <typeparam name="Content">Указывает с каким типом контента будет работать класс</typeparam>
    public class AssociatedPages : List<Page>, IEnumerable<Page>
    {
        private string linkSelector;
        private string uriPattern;
        private string contentBlock;

        /// <summary>
        /// Свойство, хранящее сслыки на вложенные страницы
        /// </summary>
        public List<Link> Links { get; } = new List<Link>();

        public AssociatedPages GetLinks()
        {
            foreach (var page in this)
            {
                foreach (var elem in new Content().GetContent(page.HtmlDocument.DocumentNode, linkSelector).HtmlElements)
                {
                    Links.Add(new Link(elem.Attributes["href"].Value));

                    Console.WriteLine(Links[Links.Count - 1].Uri);
                }
            }
            return this;
        }

        public string LinkSelector { get => linkSelector; set => linkSelector = value; }
        public string UriPattern { get => uriPattern; set => uriPattern = value; }
        public string ContentBlock { get => contentBlock; set => contentBlock = value; }

        public AssociatedPages(string uriPattern)
        {
            this.uriPattern = uriPattern;
        }

        /// <summary>
        /// Конструктор, который создает объект класса AssociatedPages
        /// </summary>
        /// <param name="uriPattern">Общий вид ссылок</param>
        /// <param name="linkSelector">CSS селектор, который указывает на ссылки</param>
        /// <param name="contentBlock">CSS селектор, который указывает на блок, содержащий основную информацию</param>
        public AssociatedPages(string uriPattern, string linkSelector, string contentBlock = "") : this(uriPattern)
        {
            this.linkSelector = linkSelector;
            this.contentBlock = contentBlock;
        }

        public void Fill(Action<Page> debug)
        {
            PageEnumerator pageEnumerator = new PageEnumerator(uriPattern, contentBlock);
            while (pageEnumerator.MoveNext())
            {
                this.Add(pageEnumerator.Current);

                debug(pageEnumerator.Current);
            }

            /*foreach (var page in this)
            {
                this.Add(page);
                debug(page);
            }*/
        }



        /*public new IEnumerator<Page> GetEnumerator()
        {
            return new PageEnumerator(uriPattern, contentBlock);
        }*/

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
