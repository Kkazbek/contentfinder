﻿using HtmlAgilityPack;
using System;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;

namespace ContentFinder.Core
{
    /// <summary>
    /// Класс, который позволяет скачивать страницу из указанного сайта
    /// </summary>
    public class Page : IDisposable
    {
        // возможно с данным полем придется еще поработать,
        // т.к. может быть придется парсить с нескольких сайтов
        internal static string host = "";
        private static WebRequest webRequest;
        private static WebResponse webResponse;
        private Uri oldLinkPage;
        private Uri linkToPage;
        private HtmlDocument htmlDocument = new HtmlDocument() { OptionComputeChecksum = false };

        private StringBuilder webDocument;

        /// <summary>
        /// Свойство, которое хранит страницу в виде DOM
        /// </summary>
        public HtmlDocument HtmlDocument
        {
            get
            {
                htmlDocument.LoadHtml(WebDocument.ToString());
                // Убираем script и noscript
                /*foreach (var el in htmlDocument.DocumentNode.SelectNodes("//script"))
                {
                    el.Remove();
                }*/
                foreach (var el in htmlDocument.DocumentNode.SelectNodes("//noscript"))
                {
                    el.Remove();
                }
                return htmlDocument;
            }
        }

        /// <summary>
        /// Свойство, которое хранит страницу в виде текста
        /// </summary>
        public StringBuilder WebDocument
        {
            get
            {
                // Если обращение происходит по той же ссылке, то просто возвращаем уже существующий веб-контент,
                // иначе получаем веб-контент из источника
                if (oldLinkPage != null && LinkToPage.AbsoluteUri == oldLinkPage.AbsoluteUri)
                {
                    return webDocument;
                }
                webDocument.Clear().Append(this.GetPageDocument(this.LinkToPage));
                oldLinkPage = new Uri(LinkToPage.ToString());
                return webDocument;
            } 
        }

        /// <summary>
        /// Свойство, которое хранит ссылку на страницу сайта
        /// </summary>
        public Uri LinkToPage
        {
            get
            {
                return linkToPage;
            }
            set
            {
                linkToPage = value;
                host = $"{LinkToPage.Scheme}://{LinkToPage.Host}";
            }
        }

        /// <summary>
        /// Конструктор, который создает объект класса Page
        /// </summary>
        public Page()
        {
            webDocument = new StringBuilder(50000);
        }

        /// <summary>
        /// Конструктор, который создает объект класса Page
        /// </summary>
        /// <param name="linkToPage">Ссылка на страницу</param>
        public Page(Uri linkToPage) : this()
        {
            LinkToPage = linkToPage;
        }

        /// <summary>
        /// Конструктор, который создает объект класса Page
        /// </summary>
        /// <param name="linkToPage">Ссылка на страницу</param>
        public Page(string linkToPage) : this()
        {
            this.SetLinkToPage(linkToPage);
        }

        // метод работает в основном потоке
        private string GetPageDocument(Uri linkToPage)
        {
            if (linkToPage == null)
            {
                throw new NullReferenceException("Не указана ссылка на страницу");
            }
            string resDocument = "";
            try
            {
                webDocument.Clear();
                webRequest = WebRequest.CreateHttp(linkToPage);
                webResponse = webRequest.GetResponse();

                // читаем ответ
                using (StreamReader stream = new StreamReader(webResponse.GetResponseStream()))
                {
                    resDocument = stream.ReadToEnd();
                }
            }
            catch (WebException e)
            {
                var response = e.Response as HttpWebResponse;
                if (response == null)
                {
                    throw new WebException("Такой хост не найден", e);
                }
                switch (response.StatusCode)
                {
                    case HttpStatusCode.TooManyRequests:
                        // Ждем пока сервер опять не разрешит отправлять запрос
                        int delay = int.Parse(e.Response.Headers[HttpResponseHeader.RetryAfter]) * 1000;
                    
                        Thread.Sleep(delay);
                        // пробуем еще раз достать страницу
                        resDocument = this.GetPageDocument(linkToPage);
                        break;
                    case HttpStatusCode.NotFound:
                        return "";
                    default:
                        // другая ошибка
                        throw new WebException("Похоже что-то не так с сайтом", e);
                }
            }

            return resDocument;
        }

        public void Dispose()
        {
            webDocument.Clear();
            webDocument = null;
        }
    }
}
