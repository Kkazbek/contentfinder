﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using HtmlAgilityPack;

namespace ContentFinder.Core
{
    public class Content
    {
        public HtmlNodeCollection HtmlElements { get; set; }

        public Content GetContent(HtmlNode htmlDocument, string selector)
        {
            HtmlElements = htmlDocument.SelectNodes(selector) ;
            return this;
        }
    }
}
