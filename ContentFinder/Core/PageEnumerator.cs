﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using HtmlAgilityPack;

namespace ContentFinder.Core
{
    /// <summary>
    /// Класс, реализующий интерфейс IEnumerator
    /// </summary>
    /// <typeparam name="Page">Указывает с каким типом страниц будет работать класс</typeparam>
    public class PageEnumerator : IEnumerator<Page>
    {
        private string uriPattern;
        private string contentBlock;
        private int start;
        private int increment;
        private int stop = -1;
        // {number:number[:number]}
        private static Regex paginatorExpression = new Regex(@"{\d+:\d+(:\d+)?}");
        private Match match;

        private Page Old { get; set; }
        public Page Current { get; set; }

        object IEnumerator.Current => Current;

        public PageEnumerator(string uriPattern, string contentBlock)
        {
            this.uriPattern = uriPattern;
            this.contentBlock = contentBlock;
            Reset();
        }

        public void Dispose()
        {
            
        }

        public bool MoveNext()
        {
            Current = new Page(GetUri());

            if (stop != -1  && start > stop)
            {
                Reset();
                return false;
            }
            start += increment;

            // ОЧЕНЬ МЕДЛЕННО СРАВНИВАЕТ
            string currentBlock = TryGetContentBlock(Current.HtmlDocument);
            string oldBlock = (Old != null) ? TryGetContentBlock(Old.HtmlDocument) : "";
            if (String.Equals(  currentBlock,
                                oldBlock,
                                StringComparison.OrdinalIgnoreCase))
            {
                Reset();
                return false;
            }

            Old = Current;
            return true;
        }

        private string TryGetContentBlock(HtmlDocument document)
        {
            StringBuilder stringBuilder = new StringBuilder(5000);
            
            if (contentBlock == null)
            {
                stringBuilder.Append(document.DocumentNode.SelectSingleNode("//body").InnerHtml);
            }
            else
            {
                stringBuilder.Append(document.DocumentNode.SelectSingleNode(contentBlock).InnerHtml);
            }
            return stringBuilder.ToString();
        }

        public void Reset()
        {
            PaginatorInitialize();
        }

        private void PaginatorInitialize()
        {
            match = paginatorExpression.Match(uriPattern);
            if (match.Success)
            {
                string[] split = match.Groups[0].Value.Split(new char[] { '{', '}' })[1].Split(':');
                start = int.Parse(split[0]);
                increment = int.Parse(split[1]);
                if (split.Length > 2)
                {
                    stop = int.Parse(split[2]);
                }
            }
        }

        private Uri GetUri()
        {
            string uri = paginatorExpression.Replace(uriPattern, start.ToString());
            return new Uri(uri);
        }
    }
}
