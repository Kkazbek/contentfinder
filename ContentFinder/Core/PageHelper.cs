﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ContentFinder.Core
{
    internal static class PageHelper
    {
        /// <summary>
        /// Задает ссылку на страницу из строки
        /// </summary>
        /// <param name="page">Текущая страница</param>
        /// <param name="linkToPage">Строка, содержащая ссылку на страницу</param>
        internal static void SetLinkToPage(this Page page, string linkToPage)
        {
            try
            {
                page.LinkToPage = new Uri(linkToPage);
            }
            catch (UriFormatException)
            {
                if (Page.host != null)
                {
                    page.LinkToPage = new Uri(Page.host + linkToPage);
                }
                else
                {
                    throw new UriFormatException($"Строка \"{linkToPage}\" не является ссылкой на страницу");
                }
            }
        }
    }
}
