﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ContentFinder.Core
{
    public class Link: Content
    {
        public string Uri { get; set; }

        public Link()
        {

        }

        public Link(string uri)
        {
            Uri = uri;
        }
    }
}
