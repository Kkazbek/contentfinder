﻿using ContentFinder.ModelBuilder;
using ContentFinder.ModelBuilder.Internal;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace ContentFinder.Example.MangaModel
{
    class MangaContext : CFContext
    {
        public CFSet<Manga> Mangas { get; set; }
        public CFSet<Chapter> Chapters { get; set; }

        protected override void OnConfiguring(CFContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseUri(new Uri("https://mangaclub.ru"))
                .ToJson("manga.json");
        }
        
        protected override void OnModelCreating(CFModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Manga>()
                .ListedIn("{this}/mangas/page/{1:1:1}/")
                .HasLinkSelector("//div[@id='dle-content']/div[@class='short-story']//h2[@class='title']/a");

            modelBuilder.Entity<Manga>()
                .IsRoot()
                .HasDescendant<Chapter>()

                .Property(m => m.EnglishName)
                .HasSelector("//div[@id='dle-content']//h1[@class='title']")
                .Select(c =>
                {
                    return c.HtmlElements.First().InnerText.Split('/')[0].Trim();
                })

                .Property(m => m.RussianName)
                .HasSelector("//div[@id='dle-content']//h1[@class='title']")
                .Select(c =>
                {
                    var names = c.HtmlElements.First().InnerText.Split('/');
                    // Если название есть
                    return (names.Count() == 2) ? names[1] : "";
                })

                .Property(m => m.JapaneseName)
                .HasSelector("//div[@id='dle-content']//h1[@class='title']")
                .Select(c =>
                {
                    var names = c.HtmlElements.First().InnerText.Split('/');
                    // Если название есть
                    return (names.Count() == 3) ? names[2] : "";
                })

                .Property(m => m.Genres)
                .HasSelector("//div[@id='dle-content']//div[@class='more-info']/br[position()=1]/preceding-sibling::span[position()!=last()]")
                .Select(c =>
                {
                    List<string> genres = new List<string>();
                    foreach (var elem in c.HtmlElements)
                    {
                        genres.Add(elem.InnerText);
                    }
                    return genres;
                })

                .Property(m => m.YearOfPrinting)
                .HasSelector("//div[@id='dle-content']//div[@class='more-info']/span[@class='info' and text()='Год печати']/following-sibling::a[position()=1]")
                .Select(c => int.Parse(c.HtmlElements.First().InnerText))

                .Property(m => m.Autors)
                .HasSelector(   "//div[@class='more-info']/span[@class='info' and text()='Автор(ы)']/following-sibling::a | " +
                                "//div[@class='more-info']/span[@class='info' and text()='Автор(ы)']/following-sibling::br")
                .Select(c =>
                {
                    List<string> autors = new List<string>();
                    foreach (var elem in c.HtmlElements)
                    {
                        if (elem.Name != "br")
                        {
                            autors.Add(elem.InnerText);
                        }
                        else
                        {
                            break;
                        }
                    }
                    return autors;
                })

                .Property(m => m.Translators)
                .HasSelector(   "//div[@class='more-info']/span[@class='info' and text()='Перевод']/following-sibling::a | " +
                                "//div[@class='more-info']/span[@class='info' and text()='перевод']/following-sibling::br")
                .Select(c =>
                {
                    List<string> translators = new List<string>();
                    foreach (var elem in c.HtmlElements)
                    {
                        if (elem.Name != "br" && elem.InnerText != "ссылка")
                        {
                            translators.Add(elem.InnerText);
                        }
                        else
                        {
                            break;
                        }
                    }
                    return translators;
                })

                .Property(m => m.Description)
                .HasSelector("//div[@id='dle-content']//div[@class='description_manga']/child::* | //div[@id='dle-content']//div[@class='description_manga']/child::text()")
                .Select(c =>
                {
                    string res = "";
                    foreach (var child in c.HtmlElements)
                    {
                        res += child.InnerText + '\n';
                    }
                    // без последнего перевода строки
                    return res.Remove(res.Length - 2);
                });

            modelBuilder.Entity<Chapter>()
                .ListedIn("{this}{parent}")
                .HasLinkSelector("//div[@id='dle-content']//div[@class='chapter-main']//div[@class='chapter-namber']/a")
                .SetLimitBlock("//div[@id='dle-content']");

            modelBuilder.Entity<Chapter>()
                .Property(c => c.Name)
                .HasSelector("//head/title")
                .Select(c =>
                {
                    string title = c.HtmlElements.First().InnerText;
                    return title.Substring(title.IndexOf("Глава:"));
                });

            modelBuilder.Entity<Chapter>()
                .Property(c => c.Pages)
                .HasSelector("//div[@id='dle-content']//div[contains(@class, 'head-body')]/div[@class='manga-lines-page']/a")
                .Select(c =>
                {
                    List<string> pages = new List<string>();
                    foreach (var elem in c.HtmlElements)
                    {
                        pages.Add(elem.Attributes["data-i"].Value);
                    }
                    return pages;
                });

        }
    }
}
