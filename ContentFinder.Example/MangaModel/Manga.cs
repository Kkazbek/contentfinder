﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ContentFinder.Example.MangaModel
{
    class Manga
    {
        public string       EnglishName     { get; set; }
        public string       RussianName     { get; set; }
        public string       JapaneseName    { get; set; }
        public List<string> Genres          { get; set; }
        public int          YearOfPrinting  { get; set; }
        public List<string> Autors          { get; set; }
        public List<string> Translators     { get; set; }
        public string       Description     { get; set; }

        public List<Chapter> Chapters       { get; set; }
    }
}
