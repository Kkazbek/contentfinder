﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ContentFinder.Example.MangaModel
{
    class Chapter
    {
        public string       Name  { get; set; }
        public List<string> Pages { get; set; }
    }
}
