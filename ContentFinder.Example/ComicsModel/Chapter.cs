﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ContentFinder.Example.ComicsModel
{
    public class Chapter
    {
        public string Name          { get; set; }
        public string SecondName    { get; set; }
        public string Publishing    { get; set; }
        public string YearOfRelease { get; set; }
        public string Localization  { get; set; }
        public string Series        { get; set; }

        public List<ComicsPages> ComicsPages { get; set; }
    }
}
