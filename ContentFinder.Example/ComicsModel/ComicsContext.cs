﻿using ContentFinder.ModelBuilder;
using ContentFinder.ModelBuilder.Internal;
using ContentFinder.Core;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ContentFinder.Example.ComicsModel
{
    public class ComicsContext : CFContext
    {

        public CFSet<Comics> Comics { get; set; }

        public CFSet<Chapter> Chapters { get; set; }

        public CFSet<ComicsPages> ComicsPages { get; set; }


        protected override void OnConfiguring(CFContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseUri(new Uri("http://unicomics.ru"))
                .ToJson("unicomics.json");
        }

        protected override void OnModelCreating(CFModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Comics>()
                .ListedIn("{this}/comics/series/page/{1:1:1}")
                .HasLinkSelector("//div[contains(@class, 'left_container')]//div[@class='right_comics']/a")
                .SetLimitBlock("//div[contains(@class, 'left_container')]");

            modelBuilder.Entity<Comics>()
                .IsRoot()
                .HasContainerContent("//div[@class='left_container']")
                .HasDescendant<Chapter>()

                .Property(m => m.Name)
                .HasSelector("//div[@class='info']/h1")

                .Property(m => m.SecondName)
                .HasSelector("//div[@class='info']/h2")

                .Property(m => m.Genre)
                .HasSelector("//div[@class='info']/table//tr/td[text()='Жанр:']/following-sibling::td")

                .Property(m => m.Publishing)
                .HasSelector("//div[@class='info']/table//tr/td[text()='Издательство:']/following-sibling::td")

                .Property(m => m.YearOfRelease)
                .HasSelector("//div[@class='info']/table//tr/td[text()='Год выпуска:']/following-sibling::td")
                .Select(c => int.Parse(c.HtmlElements[0].InnerText))

                .Property(m => m.Description)
                .HasSelector("//div[@class='info']/p/following-sibling::p[position()='2']");

            modelBuilder.Entity<Chapter>()
                .ListedIn("{this}{parent}/page/{1:1:1}")
                .HasLinkSelector("//div[contains(@class, 'left_container')]//div[@class='right_comics']/a");

            modelBuilder.Entity<Chapter>()
                .HasDescendant<ComicsPages>()

                .Property(c => c.Name)
                .HasSelector("//div[@class='info']/h1")

                .Property(c => c.SecondName)
                .HasSelector("//div[@class='info']/h2")

                .Property(c => c.Publishing)
                .HasSelector("//div[@class='info']/table//tr/td[text()='Издательство:']/following-sibling::td")

                .Property(c => c.YearOfRelease)
                .HasSelector("//div[@class='info']/table//tr/td[text()='Год выпуска:']/following-sibling::td")

                .Property(c => c.Localization)
                .HasSelector("//div[@class='info']/table//tr/td[text()='Локализация:']/following-sibling::td")

                .Property(c => c.Series)
                .HasSelector("//div[@class='info']/table//tr/td[text()='Серия:']/following-sibling::td");

            modelBuilder.Entity<ComicsPages>()
                .IsSinglePage("{this}{parent['issue' -> 'online']}");

            modelBuilder.Entity<ComicsPages>()
                .Property(p => p.Pages)
                .HasSelector("//script[text()[contains(., 'paginator1')]]")
                .Select(c =>
                {
                    List<string> comicsPages = new List<string>();
                    string text = c.HtmlElements.First().InnerText;
                    string countOfPage = (new Regex(@"(\d+)")).Matches(text)[1].Value;
                    AssociatedPages pages = new AssociatedPages($"http://unicomics.ru{(new Regex(@"[\w/-]+")).Matches(text)[6]}{{1:1:{countOfPage}}}");
                    pages.Fill((page) =>
                    {
                        Console.WriteLine($"Ссылка {page.LinkToPage.ToString()} добавлена");
                    });
                    foreach (var page in pages)
                    {
                        Content content = new Content();
                        comicsPages.Add(content.GetContent(page.HtmlDocument.DocumentNode, "//div[contains(@class, 'wrapper-online')]//img[contains(@class, 'image_online')]").HtmlElements.First().Attributes["src"].Value);
                    }
                    return comicsPages;
                });
        }
    }
}
