﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ContentFinder.Example.ComicsModel
{
    public class ComicsPages
    {
        public List<string> Pages { get; set; }
    }
}
