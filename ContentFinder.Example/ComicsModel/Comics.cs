﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ContentFinder.Example.ComicsModel
{
    public class Comics
    {
        public string Name          { get; set; }
        public string SecondName    { get; set; }
        public string Genre         { get; set; }
        public string Publishing    { get; set; }
        public int    YearOfRelease { get; set; }
        public string Description   { get; set; }

        public List<Chapter> Chapters { get; set; }
    }
}
