﻿using ContentFinder.Example.MangaModel;
using ContentFinder.Example.ComicsModel;
using ContentFinder.ModelFactory;
using System;

namespace ContentFinder.Example
{
    class Program
    {
        static void Main(string[] args)
        {
            ComicsContext comicsContext = new CFModelFactory<ComicsContext>().Build();

            MangaContext mangaContext = new CFModelFactory<MangaContext>().Build();

            Console.ReadLine();
        }
    }
}
